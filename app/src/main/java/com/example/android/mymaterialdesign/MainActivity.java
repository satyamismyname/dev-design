package com.example.android.mymaterialdesign;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar=findViewById(R.id.toolbar_id);
        setSupportActionBar(toolbar);
        //if above line shows error,import android.support.v7.widget.Toolbar

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       /* this single line displays the back button at the toolbar.
        this back button's id is android.R.id.home.*/
    }
    //the below 2 methods are required to add menu in the toolbar.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.item1_id){
            Toast.makeText(MainActivity.this,"item1 clicked!",Toast.LENGTH_SHORT).show();
        }else if(id==R.id.item2_id){
            Toast.makeText(MainActivity.this,"item2 clicked!",Toast.LENGTH_SHORT).show();
        }else if(id==R.id.item3_id){
            Toast.makeText(MainActivity.this,"item3 clicked!",Toast.LENGTH_SHORT).show();
        }else if(id==R.id.cart_id) {
            Toast.makeText(MainActivity.this, "CART icon clicked!", Toast.LENGTH_SHORT).show();
        }else if(id==R.id.inbuilt_app_bar_search_id) {
            Toast.makeText(MainActivity.this, "Search icon clicked!", Toast.LENGTH_SHORT).show();
        }else if(id==android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
